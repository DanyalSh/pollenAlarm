@extends('admin.layout')
@section('content')

    <div class="row">

        <div class="portlet-body">
            <div class="table-scrollable">
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Email</th>
                        <th>Location</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($subscriptions as $key=>$subscription)
                        <tr id="row_id_{{$subscription->id}}">
                            <td>{{$key+1}}</td>
                            <td>{{$subscription->email}}</td>
                            <td>{{$subscription->location}}</td>
                            <td>
                                <a href="/admin/users/{{$subscription->id}}" data-toggle="tooltip" data-original-title="Edit">
                                    <i class="fa fa-pencil text-inverse m-r-10"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection