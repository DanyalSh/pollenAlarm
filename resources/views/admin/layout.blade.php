<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8"/>
	<title>Pollen Alert | {{$page}}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport"/>
	<meta content="" name="description"/>
	<meta content="" name="author"/>


	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700|" rel="stylesheet" type="text/css">
	<link href="{{ asset('fonts/font-awesome.min.css') }}" rel="stylesheet" type="text/css">

	<link rel="stylesheet" href="{{ asset('css/admin/bootstrap.min.css') }}">

	<link rel="stylesheet" href="{{ asset('css/admin/components.css') }}">
	<link rel="stylesheet" href="{{ asset('css/admin/plugins.css') }}">
	<link rel="stylesheet" href="{{ asset('css/admin/layout.css') }}">
	<link rel="stylesheet" href="{{ asset('css/admin/grey.css') }}">
	<link rel="stylesheet" href="{{ asset('css/admin/custom.css') }}">

	<style>
		.page-container-bg-solid .page-title {
			margin-top: 0px !important;
			margin-bottom: 50px !important;
		}
	</style>
</head>

<body class="page-boxed page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-closed-hide-logo">
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top" style="background:#fff;">
	<!-- BEGIN HEADER INNER -->
	<div class="page-header-inner container">

		<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
		</a>

		<div class="page-top">

			<div class="top-menu">
				<ul class="nav navbar-nav pull-right">
					<li class="dropdown dropdown-user">
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">

                            <span class="username username-hide-on-mobile">
            {{\Auth::user()->name}} </span>
							<i class="fa fa-angle-down"></i>
						</a>
						<ul class="dropdown-menu dropdown-menu-default">
							<li>
								<a href="{{route('admin.dashboard')}}">
									<i class="icon-user"></i> My Profile </a>
							</li>
							<li>
								<a href="{{route('logout')}}">
									<i class="icon-key"></i> Log Out </a>
							</li>
						</ul>
					</li>
					<!-- END USER LOGIN DROPDOWN -->
				</ul>
			</div>
			<!-- END TOP NAVIGATION MENU -->
		</div>
		<!-- END PAGE TOP -->
	</div>
	<!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>

<div class="container">
	<div class="page-container">

		<div class="page-sidebar-wrapper">

			<div class="page-sidebar navbar-collapse collapse">

				<ul class="page-sidebar-menu page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
					<li class="start {{ (\Request::route()->getName() == 'admin.dashboard') ? 'active' : '' }} ">
						<a href="{{route('admin.dashboard')}}">
							<span class="title">Dashboard</span>
                            {!! (\Request::route()->getName() == 'admin.dashboard') ? '<span class="selected"></span>' : '' !!}
						</a>
					</li>
					<li class="start {{ (\Request::route()->getName() == 'admin.users' || \Request::route()->getName() == 'admin.editUsers') ? 'active' : '' }} ">
						<a href="{{route('admin.users')}}">
							<span class="title">Users</span>
                            {!! (\Request::route()->getName() == 'admin.users' || \Request::route()->getName() == 'admin.editUsers') ? '<span class="selected"></span>' : '' !!}
						</a>
					</li>
				</ul>
				<!-- END SIDEBAR MENU -->
			</div>
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
				<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
								<h4 class="modal-title">Modal title</h4>
							</div>
							<div class="modal-body">
								Widget settings form goes here
							</div>
							<div class="modal-footer">
								<button type="button" class="btn blue">Save changes</button>
								<button type="button" class="btn default" data-dismiss="modal">Close</button>
							</div>
						</div>
						<!-- /.modal-content -->
					</div>
					<!-- /.modal-dialog -->
				</div>

				<h3 class="page-title">
					{{$page}}</h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">

                        @php
                            if(in_array(\Request::route()->getName(), array('admin.dashboard','admin.users')))
                            {
                            echo '<li><i class="fa fa-home"></i><a href="'.route(\Request::route()->getName()).' ">'.$page.' </a></li>';
                            }

                            if(in_array(\Request::route()->getName(), array('admin.editUsers')))
                            {
                            echo '<li><i class="fa fa-home"></i><a href="'.route("admin.users").'">Users </a></li>
                             <i class="fa fa-angle-right"> </i> <li> <a href="'.url()->current().'">'.$page.'</a></li>';
                            }

                        @endphp
					</ul>
				</div>
            
            @yield('content')

			</div>
		</div>
	</div>

</div>

<script>
    var accuweatherForeCastApiKey = '{{config('services.accuweather.forecastKey')}}';
    var accuweatherLocationApiKey = '{{config('services.accuweather.locationKey')}}';
</script>

<script src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>
<script src="{{ asset('js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>

@yield('pageScripts')
@show
</body>
</html>



