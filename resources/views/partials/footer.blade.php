<footer class="site-footer">

	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<form action="/subscription" class="subscribe-form" method="post" id="subscription_form">
					{{csrf_field()}}
					<input type="hidden" value="" name="location_id" id="location_id">
					<input type="hidden" value="" name="location_name" id="location_name">
					<input type="email" required="required" placeholder="Enter your email to subscribe..." name="email" id="subscription_email">
					<input type="button" value="Subscribe" id="subscription_button">
				</form>
			</div>
			<div class="col-md-3 col-md-offset-1">
				<div class="social-links">
					<a href="#"><i class="fa fa-facebook"></i></a>
					<a href="#"><i class="fa fa-twitter"></i></a>
					<a href="#"><i class="fa fa-google-plus"></i></a>
					<a href="#"><i class="fa fa-pinterest"></i></a>
				</div>
			</div>
		</div>

		<p class="colophon">Copyright 2014 Company name. All rights reserved</p>
	</div>

	<div class="modal fade" id="subscribtionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header" style="color:black">
					<h5 class="modal-title" id="exampleModalLabel">Confirm Location</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" id="modal-body" style="color:black">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal" id="subscription_change_location">Yes</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal" id="subscription_confirmed">No</button>
				</div>
			</div>
		</div>
	</div>
</footer> <!-- .site-footer -->
