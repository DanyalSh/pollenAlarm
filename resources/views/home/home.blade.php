@extends('layouts.app')
@section('content')
	<style>
		.ui-combobox {
			position: relative;
			display: inline-block;
		}
		.ui-combobox-toggle {
			position: absolute;
			top: 0;
			bottom: 0;
			margin-left: -1px;
			padding: 0;
			/* support: IE7 */
			*height: 1.7em;
			*top: 0.1em;
		}
		.ui-combobox-input {
			margin: 0;
			padding: 0.3em;
		}
	</style>


	<div class="hero" data-bg-image="{{ asset('images/banner-new.jpg') }}">
		<div class="container">
			<form action="#" class="find-location">
				<input type="text" id="awxSearchTextBox" name="awxSearchTextBox" class="awxSearchTextBox" placeholder="Find your location...">
				{{--<input type="submit" id="awxSearchButton" value="Find">--}}
			</form>
		</div>
	</div>
	<div class="forecast-table">
		<div class="container">
			<div class="forecast-container">
				<div class="today forecast">
					<div class="forecast-header">
						<div class="day">{{ \Carbon::now()->format('l') }}</div>
						<div class="date">{{ \Carbon::now()->format('d M') }}</div>
					</div> <!-- .forecast-header -->
					<div class="forecast-content">
						<div class="location" id="location"></div>
						<div class="degree">
							<div class="num" id="current-max-temprature"></div>
							<div class="forecast-icon">
								<img src="{{ asset('images/icons/icon-1.svg') }}" alt="" width=90>
							</div>
							<div>
							<small id="current-min-temprature" style="float:left;"></small>

						<table border="0" width="50%" style="font-size: 14px !important; font-weight: normal !important; float: right;">
							<tr>
								<td width="30%" align="left">Air Quality</td>
								<td width="15%" align="center">:</td>
								<td width="35%"><span id="pollen-air-quality-current"> - </span></td>
							</tr>
							<tr>
								<td width="30%" align="left">Grass</td>
								<td width="15%" align="center">:</td>
								<td width="35%"><span id="pollen-grass-quality-current"> - </span></td>
							</tr>
							<tr>
								<td width="30%" align="left">Mold</td>
								<td width="15%" align="center">:</td>
								<td width="35%"><span id="pollen-mold-quality-current"> - </span></td>
							</tr>
							<tr>
								<td width="30%" align="left">Regweed</td>
								<td width="15%" align="center">:</td>
								<td width="35%"><span id="pollen-regweed-quality-current"> - </span></td>
							</tr>
							<tr>
								<td width="30%" align="left">Tree</td>
								<td width="15%" align="center">:</td>
								<td width="35%"><span id="pollen-tree-quality-current"> - </span></td>
							</tr>
						</table>
					</div>
					</div>
					<div style="float:left; margin-top: 15px; margin-bottom: 15px;">
						<span><img src="{{ asset('images/icon-umberella.png') }}" alt="">20%</span>
						<span><img src="{{ asset('images/icon-wind.png') }}" alt="">18km/h</span>
						<span><img src="{{ asset('images/icon-compass.png') }}" alt="">East</span>
					</div>
					</div>
				</div>
				@for($i = 1; $i < 5; $i++)
					<div class="forecast">
						<div class="forecast-header">
							<div class="day">{{ \Carbon::now()->addDays($i)->format('l')  }}</div>
						</div> <!-- .forecast-header -->
						<div class="forecast-content">
							<div class="forecast-icon">
								<img src="{{ asset('images/icons/icon-3.svg') }}" alt="" width=48>
							</div>
							<div class="degree"><span id="max-temprature-{{$i}}">23</span></div>
							<small><span id="min-temprature-{{$i}}">18</span></small><br />
							<table border="0" width="100%" style="margin-top: 15px;">
								<tr>
									<td width="50%" align="left">Air Quality</td>
									<td width="5%" align="center">:</td>
									<td width="35%"><span id="pollen-air-quality-{{$i}}"> - </span></td>
								</tr>
								<tr>
									<td width="50%" align="left">Grass</td>
									<td width="5%" align="center">:</td>
									<td width="35%"><span id="pollen-grass-quality-{{$i}}"> - </span></td>
								</tr>
								<tr>
									<td width="50%" align="left">Mold</td>
									<td width="5%" align="center">:</td>
									<td width="35%"><span id="pollen-mold-quality-{{$i}}"> - </span></td>
								</tr>
								<tr>
									<td width="50%" align="left">Regweed</td>
									<td width="5%" align="center">:</td>
									<td width="35%"><span id="pollen-regweed-quality-{{$i}}"> - </span></td>
								</tr>
								<tr>
									<td width="50%" align="left">Tree</td>
									<td width="5%" align="center">:</td>
									<td width="35%"><span id="pollen-tree-quality-{{$i}}"> - </span></td>
								</tr>
							</table>
						</div>
					</div>
				@endfor
			</div>
		</div>
		<div class="container" style="text-align: center;">
			<p><strong>Headline: </strong>"<span id="headline"></span>"</p>
		</div>
	</div>



@endsection

@section('pageScripts')
<script>

	window.locationQuery = '{{ $location->city . ',' .  $location->state }}';
	getZip();

	$(document).on("click","#subscription_button",function(){

	    var email = $("#subscription_email").val();

	    if(email == ""){
	        return false;
		}

	    var searchFieldValue = $("#awxSearchTextBox").val();

	    if(searchFieldValue == ""){
            searchFieldValue = $("#location").text();
		}

		$("#modal-body").html("your current Location: <strong><i>"+searchFieldValue+" </i></strong>. " +
			"<p> Note: Email will be sent on basis of your Location</p>" +
			"<p> Do you want to change the location? </p>");

        $("#subscribtionModal").modal("show");
	});

	$(document).on("click","#subscription_confirmed", function () {

		$("#subscription_form").submit();
    });

	$(document).on("click","#subscription_change_location", function (e) {

        e.preventDefault();

        $('html, body').animate({
            scrollTop: $("#awxSearchTextBox").offset().top - 50
        }, 1000, function() {
            $("input[name='awxSearchTextBox']").focus();
        });

    });

</script>

@endsection
