<?php

namespace App\Console;

use App\Traits\HelperTrait;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Models\Subscription;
use App\Notifications\pollenAlertNotification;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    use HelperTrait;

    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        $schedule->call(function () {

            $subscriptions = Subscription::where('status', 1)->get();

            foreach ($subscriptions as $subscription) {

                $email = $subscription->email;
                $location = $subscription->location;

                $curlResponse = self::accuweatherCurlRequest($location);

                if ($curlResponse['status'] == "success") {

                    $response = $curlResponse['response'];
                    $response = ltrim($response, 'awxCallback(');
                    $response = rtrim($response, ')');

                    $jsonData = json_decode($response, true);

                    if (isset($jsonData['DailyForecasts'])) {

                        $dailyForcasts = $jsonData['DailyForecasts'];

                        $countDays = 0;

                        if (count($dailyForcasts) > 0) {

                            foreach ($dailyForcasts as $dailyForcast) {

                                $airAndPollen = $dailyForcast['AirAndPollen'];

                                $airQuality = $airAndPollen[0]['Category'];
                                $grass = $airAndPollen[1]['Category'];
                                $mold = $airAndPollen[2]['Category'];
                                $ragweed = $airAndPollen[3]['Category'];
                                $tree = $airAndPollen[4]['Category'];

                                $count = 0;
                                $message = "";

                                if (strtolower($airQuality) == 'high') {
                                    $count++;
                                    $message .= '<p>Pollen Air Quality level is <strong>High</strong></p>';
                                }
                                if (strtolower($grass) == 'high') {
                                    $count++;
                                    $message .= '<p>Pollen level in Grass is <strong>High</strong></p>';
                                }
                                if (strtolower($mold) == 'high') {
                                    $count++;
                                    $message .= '<p>Pollen level in Mold is <strong>High</strong></p>';
                                }
                                if (strtolower($ragweed) == 'high') {
                                    $count++;
                                    $message .= '<p>Pollen level in Ragweed is <strong>High</strong></p>';
                                }
                                if (strtolower($tree) == 'high') {
                                    $count++;
                                    $message .= '<p>Pollen level in Tree is <strong>High</strong></p>';
                                }

                                $dt = \Carbon::now();
                                $dt->addDay($countDays);
                                $date = $dt->format('d M Y');
                                $day = $dt->format('l');

                                if ($count > 0 && $countDays == 0) {
                                    $completeMessage = '<p><strong>Pollen Information of ' . $date . ' ,' . $day.' </strong></p>';
                                    $completeMessage .= $message;

                                    $subscription->notify(new pollenAlertNotification([
                                        'email' => $subscription->email,
                                        'message' => $completeMessage
                                    ]));
                                }

                                $countDays++;
                            }
                        }
                    }
                }
            }

        })->everyMinute();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
