<?php

namespace App\Http\Controllers;

use App\Models\Subscription;
use App\Notifications\pollenAlertNotification;
use App\Traits\HelperTrait;
use Illuminate\Http\Request;
use Torann\GeoIP\Facades\GeoIP;

class HomeController extends Controller
{
    use HelperTrait;

    public function index(Request $request){
        $req = $request->instance();
        //$request->setTrustedProxies(array('127.0.0.1')); // only trust proxy headers coming from the IP addresses on the array (change this to suit your needs)
        $myip = $req->getClientIp();
        $x = geoip($myip);
//        dd($x);
        return view('home.home')->with('location', $x);
    }

    public function contact(){
        return view('contact.contact');
    }

    public function cronEmails()
    {

        $subscriptions = Subscription::where('status', 1)->get();

        foreach ($subscriptions as $subscription) {

            $email = $subscription->email;
            $location = $subscription->location;

            $curlResponse = self::accuweatherCurlRequest($location);

            if ($curlResponse['status'] == "success") {

                $response = $curlResponse['response'];
                $response = ltrim($response, 'awxCallback(');
                $response = rtrim($response, ')');

                $jsonData = json_decode($response, true);

                $dailyForcasts = $jsonData['DailyForecasts'];

                $countDays = 0;

                if (count($dailyForcasts) > 0) {

                    foreach ($dailyForcasts as $dailyForcast) {

                        $airAndPollen = $dailyForcast['AirAndPollen'];

                        $airQuality = $airAndPollen[0]['Category'];
                        $grass = $airAndPollen[1]['Category'];
                        $mold = $airAndPollen[2]['Category'];
                        $ragweed = $airAndPollen[3]['Category'];
                        $tree = $airAndPollen[4]['Category'];

                        $count = 0;
                        $message = "";

                        if (strtolower($airQuality) == 'high') {
                            $count++;
                            $message .= '<p>Pollen Air Quality level is <strong>High</strong></p>';
                        }
                        if (strtolower($grass) == 'high') {
                            $count++;
                            $message .= '<p>Pollen level in Grass is <strong>High</strong></p>';
                        }
                        if (strtolower($mold) == 'high') {
                            $count++;
                            $message .= '<p>Pollen level in Mold is <strong>High</strong></p>';
                        }
                        if (strtolower($ragweed) == 'high') {
                            $count++;
                            $message .= '<p>Pollen level in Ragweed is <strong>High</strong></p>';
                        }
                        if (strtolower($tree) == 'high') {
                            $count++;
                            $message .= '<p>Pollen level in Tree is <strong>High</strong></p>';
                        }

                        $dt = \Carbon::now();
                        $dt->addDay($countDays);
                        $date = $dt->format('d M Y');
                        $day = $dt->format('l');

                        if ($count > 0) {
                            $completeMessage = '<p><strong>Pollen Information of ' . $date . ' ,' . $day.' </strong></p>';
                            $completeMessage .= $message;

                            $subscription->notify(new pollenAlertNotification([
                                'id' => $subscription->email,
                                'message' => $completeMessage
                            ]));
                        }

                        $countDays++;
                    }
                 }
            }
        }
    }
}
