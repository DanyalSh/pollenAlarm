<?php

namespace App\Http\Controllers;

use App\Models\Subscription;
use App\Traits\ValidateTrait;
use Illuminate\Http\Request;
use Auth;
class AdminController extends Controller
{
    use ValidateTrait;

    public function index()
    {
        if (Auth::check()) {
            return redirect()->route('admin.dashboard');
        }
        return view('admin.login');
    }
    public function dashboard()
    {
        $page = "Dashboard";

        if (Auth::check()) {
            return view('admin.dashboard',compact('page'));
        }
        return view('admin.login');
    }

    public function login(Request $request)
    {
        $attributes = $request->all();

        $validator = self::validateLoginForm($attributes);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator->errors());
        }

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return redirect()->route('admin.dashboard');

        } else {

            \Session::flash('error','Invalid Credientails');
            return redirect()->back()->withInput();
        }
    }

    public function loadUsersView()
    {
        $page = "Users";

        $subscriptions = Subscription::all();

        return view('admin.users',compact('subscriptions','page'));
    }

    public function loadEditUsersView($id)
    {
        $page = "User Detail";

        $subscription = Subscription::find($id);

        return view('admin.user-detail',compact('subscription','page'));

//        dd($subscription);
    }
}
