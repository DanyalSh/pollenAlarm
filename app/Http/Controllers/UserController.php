<?php

namespace App\Http\Controllers;

use App\Models\Subscription;
use App\Notifications\SubscriptionNotification;
use App\Traits\ValidateTrait;
use Illuminate\Http\Request;

class UserController extends Controller
{
    use ValidateTrait;

    public function subscriptionEmail(Request $request)
    {
        $attributes = $request->all();

        $validator = self::validateSubscriptionEmail($attributes);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator->errors());
        }

        $code = str_random(10);

        $subscription = new Subscription();

        $subscription->email = $attributes['email'];

        $subscription->location_id = $attributes['location_id'];

        $subscription->location = $attributes['location_name'];

        $subscription->code = $code;

        $subscription->save();

        $URL = url('confirm/email/' . encrypt($code) . '/' . encrypt($subscription->id));

        $subscription->notify(new SubscriptionNotification([
            'id' => $subscription->id,
            'url' => $URL,
            'message' => 'One step ahead for activate you account.'
        ]));

        \Session::flash('success','Subscribtion Email has been sent, Kindly Verified Your Email');

        return redirect()->to('/');
    }

    public function verifySubscriptionEmail($code,$userId,Request $request)
    {

        $code = decrypt($code);
        $userId = decrypt($userId);

        $subscription = Subscription::where('code','=', $code)->where('id','=', $userId)->first();

        if(empty($subscription)){

            $subscription->status = '1';
            $subscription->save();

            \Session::flash('success','Email Verified Successfully');

            return redirect()->to('/');
        }

        if ($subscription->status == 1) {

               \Session::flash('error','Email already Verified');
                return redirect()->to('/');
        }

        \Session::flash('error','Invalid confirmation URL');

        return redirect()->to('/');
    }
}
