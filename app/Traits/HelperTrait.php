<?php

namespace App\Traits;


trait HelperTrait
{
    public function getResponse($data = null, $message = null)
    {
        return [
            'data' => $data,
            'message' => $message
        ];
    }

    public function accuweatherCurlRequest($location)
    {
        $curl = curl_init();

        $apiKey = config('services.accuweather.forecastKey');

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://dataservice.accuweather.com/forecasts/v1/daily/5day/260624?apikey=".$apiKey."&language=en&details=true&metric=true&callback=awxCallback",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "postman-token: 9dd2bd40-f917-2a24-3396-84c08f69140f"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {

            return [
                'status' => 'error',
                'response' => $err
            ];

        } else {

            return [
                'status' => 'success',
                'response' => $response
            ];

        }
    }
}