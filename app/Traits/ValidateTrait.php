<?php

namespace App\Traits;

use Validator;

trait ValidateTrait
{
    /**
     * Booking validation
     * @param array $attributes
     * @return \Illuminate\Validation\Validator
     */

    public function validateSubscriptionEmail(array $attributes)
    {
        $validator = Validator::make($attributes, [
            'email' => 'required|email',
        ]);

        return $validator;
    }

    public function validateLoginForm(array $attributes)
    {
        $validator = Validator::make($attributes, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        return $validator;
    }

}