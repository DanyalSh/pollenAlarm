<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    'as' => 'home',
    'uses' => 'HomeController@index'
]);

Route::get('/contact', [
    'as' => 'contact',
    'uses' => 'HomeController@contact'
]);

Route::get('/confirm/email/{code}/{userId}','UserController@verifySubscriptionEmail');

Route::post('/subscription','UserController@subscriptionEmail');

Route::get('/cron','HomeController@cronEmails');

Route::get('/login','AdminController@index')->name('admin.loginPage');
Route::get('/admin/dashboard','AdminController@dashboard')->name('admin.dashboard');

Route::post('/login','AdminController@login')->name('admin.login');

Route::get('/logout',function(){
    if (\Auth::check()) {
        \Auth::logout();
    }
    return redirect()->route('admin.loginPage');
})->name('logout');

Route::get('/admin/users','AdminController@loadUsersView')->name('admin.users');
Route::get('/admin/users/{id}','AdminController@loadEditUsersView')->name('admin.editUsers');